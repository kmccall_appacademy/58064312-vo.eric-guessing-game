
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  random_number = rand(1..100)
  guesses = 0

  while true
    puts "Guess a number"
    guess = Integer(gets.chomp)
    guesses += 1


    puts "#{guess} is too low" if guess < random_number
    puts "#{guess} is too high" if guess > random_number
    if guess == random_number
      puts "That's right, the number was #{random_number}!"
      puts "You took #{guesses} guesses!"
      break
    end
  end

end
